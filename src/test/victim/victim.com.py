from flask import Flask, jsonify, send_from_directory

app = Flask(__name__, static_url_path='/static')


@app.route("/", methods=['GET', 'POST'])
def index():
    return jsonify('')


@app.route("/favicon.ico")
def favicon():
    return send_from_directory(app.static_folder, 'favicon.ico')

if __name__ == "__main__":
    app.run(port=5001, host='127.0.0.1')
