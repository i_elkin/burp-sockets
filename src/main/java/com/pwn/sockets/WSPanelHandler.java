package com.pwn.sockets;

import burp.IBurpExtenderCallbacks;
import burp.IExtensionHelpers;
import com.pwn.sockets.gui.WSPanel;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;

import static com.pwn.sockets.LocalServlet.LOCAL_PORT;
import static com.pwn.sockets.LocalServlet.LOCAL_HOST;

public class WSPanelHandler {

    static final Logger log = LogManager.getLogger(WSPanelHandler.class.getName());

    public static final String X_HOST_HEADER = "X-Host-Header";
    public static final String X_HOST_COOKIE = "X-Host-Cookie";
    public static final String X_HOST_REQUEST_HASH = "X-Host-Request-Hash";

    private final WSPanel panel;
    private final IExtensionHelpers helpers;
    private final IBurpExtenderCallbacks callbacks;
    private WSClientPool clientPool;

    public WSPanelHandler(WSPanel panel, IBurpExtenderCallbacks callbacks, WSClientPool clientPool) {
        this.panel = panel;
        this.callbacks = callbacks;
        this.clientPool = clientPool;
        this.helpers = callbacks.getHelpers();

        /**
         * Set event handler for WebSocket start button
         */
        panel.getBtnSendOnce().addActionListener(e1 -> new Thread(() -> {
            try {
                sendOnce();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }).start());

        panel.getBtnStartScan().addActionListener(e1 -> new Thread(this::startScan).start());
    }

    /**
     * Repeater handler
     */
    public void sendOnce() throws URISyntaxException {
        long dateStart = new Date().getTime();
        String host = panel.getTbxTargetHost().getText();
        String cookies = panel.getTareaCookies().getText();
        String requestBody = panel.getTareaRequest().getText();

        WSClient client = clientPool.getSingleClient(host, cookies);

        /**
         * Set response handler if needed
         */
        if (client.getMessageHandler() == null) {
            panel.getTblConnections().addRow(host, client.hashCode());
            client.setMessageHandler(
                    new MessageHandler(requestBody, host, dateStart) {
                        @Override
                        void handleMessage(String response) {
                            panel.getTareaResponse().setText(response);
                            panel.getTblRequestsLog().addRow(host, getRequestBody(), response, new Date().getTime() - getDateStart());
                        }
                    }
            );
        } else {
            client.getMessageHandler().setDateStart(dateStart);
            client.getMessageHandler().setRequestBody(requestBody);
        }

        client.sendMessage(requestBody);
    }

    /**
     * Scanner handler with fake local server
     */
    public void startScan() {
        String stringRequest = panel.getTareaRequest().getText();
        byte[] request = helpers.buildHttpMessage(
                new ArrayList<String>() {{
                    add("GET / HTTP/1.1");
                    add("Host: " + LOCAL_HOST + ":" + LOCAL_PORT);
                    add("Connection: close"); // Simple hack to incrase localhost connection speed
                    add(X_HOST_HEADER + ": " + panel.getTbxTargetHost().getText());
                    add(X_HOST_REQUEST_HASH + ": " + DigestUtils.shaHex(helpers.base64Encode(stringRequest)));
                }},
                helpers.stringToBytes(stringRequest)
        );
        callbacks.doActiveScan(LOCAL_HOST, LOCAL_PORT, false, request);
    }
}
