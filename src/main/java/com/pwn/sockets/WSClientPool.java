package com.pwn.sockets;

import java.net.URI;
import java.util.*;

public class WSClientPool {

    public final int CLIENT_SINGLE = 0x01;
    public final int CLIENT_MULTIPLE = 0x02;

    private final Map<String, WSClient> clients = new HashMap<>();

    public WSClient getSingleClient(String host, String cookies) {
        WSClient client = clients.get(host + CLIENT_SINGLE);
        if (client == null || client.getSession() == null || !client.getSession().isOpen()) {
            client = initClient(host, cookies, CLIENT_SINGLE, "");
        }

        return client;
    }

    public WSClient getMultipleClient(String host, String cookies, String requestHash) {
        WSClient client = clients.get(host + CLIENT_MULTIPLE + requestHash);
        if (client == null || client.getSession() == null || !client.getSession().isOpen()) {
            client = initClient(host, cookies, CLIENT_MULTIPLE, requestHash);
        }

        return client;
    }

    private WSClient initClient(String host, String cookies, int clientType, String request) {
        WSClient client = null;

        try {
            client = new WSClient(new URI(host));

            clients.put(host + clientType + request, client);

            client.start(Utils.extractCookiesList(cookies));
            Thread.sleep(1000); //TODO mmake trigger
        } catch (Exception e) {
            e.printStackTrace();
        }

        return client;
    }

}
