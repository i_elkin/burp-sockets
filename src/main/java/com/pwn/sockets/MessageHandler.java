package com.pwn.sockets;

import java.util.concurrent.CountDownLatch;

public abstract class MessageHandler {

    private String requestBody;
    private String host;

    private CountDownLatch latch;
    private long dateStart;

    public MessageHandler(String request, String host, long dateStart) {
        this.host = host;
        this.dateStart = dateStart;
        this.requestBody = request;
        this.latch = new CountDownLatch(1);
    }

    public String getHost() {
        return host;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public void countDown() {
        latch.countDown();
    }

    public void await() {
        try {
            if (latch.getCount() == 0) {
                latch = new CountDownLatch(1);
            }
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    abstract void handleMessage(String message);

    public void setDateStart(long dateStart) {
        this.dateStart = dateStart;
    }

    public long getDateStart() {
        return dateStart;
    }
}
