package com.pwn.sockets;

import burp.*;
import com.pwn.sockets.gui.Tab;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Date;
import java.util.regex.Pattern;

public class HttpListener implements IHttpListener {

    static final Logger log = LogManager.getLogger(HttpListener.class.getName());

    private final Tab tab;
    private final IExtensionHelpers helpers;
    private final IBurpExtenderCallbacks callbacks;
    private WSClientPool wsPool;

    Pattern PATTERN_HOST = Pattern.compile(WSPanelHandler.X_HOST_HEADER + ": (.*)");
    Pattern PATTERN_COOKIE = Pattern.compile(WSPanelHandler.X_HOST_COOKIE + ": (.*)");
    Pattern PATTERN_REQUEST_HASH = Pattern.compile(WSPanelHandler.X_HOST_REQUEST_HASH + ": (.*)");

    public HttpListener(IBurpExtenderCallbacks callbacks, Tab tab, WSClientPool wsPool) {
        this.tab = tab;
        this.wsPool = wsPool;
        this.callbacks = callbacks;
        this.helpers = callbacks.getHelpers();
    }

    @Override
    public void processHttpMessage(int toolFlag, boolean messageIsRequest, IHttpRequestResponse messageInfo) {
        if ( messageIsRequest ||
                toolFlag != IBurpExtenderCallbacks.TOOL_SCANNER &&
                toolFlag != IBurpExtenderCallbacks.TOOL_REPEATER ||
                messageInfo.getHttpService().getPort() != LocalServlet.LOCAL_PORT ||
                !messageInfo.getHttpService().getHost().equals(LocalServlet.LOCAL_HOST)) {
            return;
        }

        try {
            processResponse(messageInfo);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fuzz action be provided through the Socket client
     */
    private void processResponse(IHttpRequestResponse messageInfo) throws InterruptedException {
        long date = new Date().getTime();
        byte[] request = messageInfo.getRequest();
        String host = Utils.extractHeaderValue(PATTERN_HOST, helpers.bytesToString(request));
        String cookies = Utils.extractHeaderValue(PATTERN_COOKIE, helpers.bytesToString(request));
        String requestHash = Utils.extractHeaderValue(PATTERN_REQUEST_HASH, helpers.bytesToString(request));
        IRequestInfo requestInfo = helpers.analyzeRequest(messageInfo);

        if (host.equals("")) {
            messageInfo.setRequest(null);
            messageInfo.setResponse(null);
        }

        String requestBody = helpers.bytesToString(Arrays.copyOfRange(request, requestInfo.getBodyOffset(), request.length));

        /**
         * make new socket request
         */
        WSClient client = wsPool.getMultipleClient(host, cookies, requestHash);
        MessageInfoHandler handler = (MessageInfoHandler) client.getMessageHandler();

        if (handler == null) {
            handler = createHandler(requestBody, host, messageInfo);
            client.setMessageHandler(handler);
            tab.getPanel().getTblConnections().addRow(host, client.hashCode());
        }
        handler.setRequestBody(requestBody);
        handler.setMessageInfo(messageInfo);
        handler.setDateStart(date);

        client.sendMessage(requestBody);

        /**
         * synchronously wait until client get response
         */
        handler.await();
    }

    private MessageInfoHandler createHandler(String requestBody, String host, IHttpRequestResponse messageInfo) {
        return new MessageInfoHandler(requestBody, host, messageInfo, 0) {

            @Override
            void handleMessage(String message) {
                IHttpRequestResponse messageInfo = getMessageInfo();

                byte[] response = messageInfo.getResponse();
                IResponseInfo responseInfo = helpers.analyzeResponse(response);
                String newResponse = helpers.bytesToString(Arrays.copyOfRange(response, 0, responseInfo.getBodyOffset()));

                getMessageInfo().setResponse(helpers.stringToBytes(newResponse + message));

                tab.getPanel().getTblRequestsLog().addRow(getHost(), getRequestBody(), message, new Date().getTime() - getDateStart());
            }

        };
    }

}
