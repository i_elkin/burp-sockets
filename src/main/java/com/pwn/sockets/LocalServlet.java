package com.pwn.sockets;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class LocalServlet extends HttpServlet {

    public static final Integer LOCAL_PORT = 8089;
    public static final String LOCAL_HOST = "localhost";

    static final Logger log = LogManager.getLogger(LocalServlet.class.getName());

    public LocalServlet() {
        try {
            this.createContext();
        } catch (Exception e) {
            log.error("Can't create local servlet context: " + e.getMessage());
        }
    }

    private void createContext() throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(LOCAL_PORT), 0);
        server.createContext("/", this::handle);
        server.setExecutor(null);
        server.start();
    }

    private void handle(HttpExchange httpExchange) throws IOException {
        String response = "{\"data\": \"JSON\"}";
        httpExchange.sendResponseHeaders(200, response.length());
        OutputStream os = httpExchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

}
