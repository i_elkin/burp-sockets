package com.pwn.sockets;

import burp.IHttpRequestResponse;

public abstract class MessageInfoHandler extends MessageHandler {

    private IHttpRequestResponse messageInfo;

    public MessageInfoHandler(String requestBody, String host, IHttpRequestResponse messageInfo, long dateStart) {
        super(requestBody, host, dateStart);
        this.messageInfo = messageInfo;
    }

    public void setMessageInfo(IHttpRequestResponse messageInfo) {
        this.messageInfo = messageInfo;
    }

    public IHttpRequestResponse getMessageInfo() {
        return messageInfo;
    }
}
