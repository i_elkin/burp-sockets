package com.pwn.sockets.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class WSConnectionsTable extends JTable {

    private final DefaultTableModel localModel;

    public WSConnectionsTable() {
        super();
        this.localModel = new DefaultTableModel();
        localModel.addColumn("");
        localModel.addColumn("Host");
        localModel.addColumn("Client Hash");

        setModel(localModel);
        getColumnModel().getColumn(0).setMaxWidth(50);
    }

    public void addRow(String host, int clientHash) {
        localModel.insertRow(0, new Object[]{true, host, clientHash});
    }

    @Override
    public Class getColumnClass(int column) {
        switch (column) {
            case 0:
                return Boolean.class;
            default:
                return String.class;
        }
    }
}
