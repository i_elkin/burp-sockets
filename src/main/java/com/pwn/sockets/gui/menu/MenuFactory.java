package com.pwn.sockets.gui.menu;

import burp.*;
import com.pwn.sockets.Utils;
import com.pwn.sockets.gui.Tab;
import com.pwn.sockets.gui.WSPanel;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class MenuFactory implements IContextMenuFactory {

    private final Tab tab;
    private final IExtensionHelpers helpers;
    private final IBurpExtenderCallbacks callbacks;

    Pattern PATTERN_HOST = Pattern.compile("Host: (.*)");
    Pattern PATTERN_COOKIE = Pattern.compile("Cookie: (.*)");

    public MenuFactory(IBurpExtenderCallbacks callbacks, Tab tab) {
        this.callbacks = callbacks;
        this.tab = tab;
        this.helpers = callbacks.getHelpers();
    }

    @Override
    public List<JMenuItem> createMenuItems(IContextMenuInvocation invocation) {
        if (invocation.getInvocationContext() != IContextMenuInvocation.CONTEXT_MESSAGE_EDITOR_REQUEST &&
                invocation.getInvocationContext() != IContextMenuInvocation.CONTEXT_MESSAGE_VIEWER_REQUEST
                ) {
            return null;
        }

        JMenuItem menuItem = new JMenuItem(" - Send to Web Socket Scanner");
        menuItem.addActionListener(e -> new Thread(() -> {
            handleMenu(invocation.getSelectedMessages()[0]);
        }).start());

        return new ArrayList<JMenuItem>() {{
            add(menuItem);
        }};
    }

    private void handleMenu(IHttpRequestResponse requestResponse) {
        WSPanel panel = tab.getPanel();
        byte[] request = requestResponse.getRequest();
        String stringRequest = helpers.bytesToString(request);
        IRequestInfo requestInfo = helpers.analyzeRequest(requestResponse);

        String host = Utils.extractHeaderValue(PATTERN_HOST, stringRequest);
        String cookieString = Utils.extractHeaderValue(PATTERN_COOKIE, stringRequest);
        String requestBody = helpers.bytesToString(Arrays.copyOfRange(request, requestInfo.getBodyOffset(), request.length));

        panel.getTareaRequest().setText(requestBody);
        panel.getTareaCookies().setText(cookieString);
        panel.getTbxTargetHost().setText("ws://" + host);
    }
}
