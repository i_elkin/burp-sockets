package com.pwn.sockets.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class WSRequestsTable extends JTable {

    private final DefaultTableModel localModel;

    public WSRequestsTable() {
        super();
        this.localModel = new DefaultTableModel();
        localModel.addColumn("");
        localModel.addColumn("Host");
        localModel.addColumn("RequestBody");
        localModel.addColumn("ResponseBody");
        localModel.addColumn("Time Spent");

        setModel(localModel);
        getColumnModel().getColumn(0).setMaxWidth(50);
        getColumnModel().getColumn(1).setMaxWidth(250);
        getColumnModel().getColumn(4).setMaxWidth(70);
    }

    public void addRow(String host, String requestBody, String responseBody, long timeSpent) {
        localModel.insertRow(0, new Object[]{true, host, requestBody, responseBody, timeSpent});
    }

    @Override
    public Class getColumnClass(int column) {
        switch (column) {
            case 0:
                return Boolean.class;
            default:
                return String.class;
        }
    }
}
