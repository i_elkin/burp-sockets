package com.pwn.sockets.gui;

import burp.IBurpExtenderCallbacks;
import burp.ITab;
import com.pwn.sockets.WSClientPool;
import com.pwn.sockets.WSPanelHandler;

import javax.swing.*;
import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tab implements ITab {

    private JFrame frame;
    private WSPanel panel;
    private WSPanelHandler handler;

    public Tab(IBurpExtenderCallbacks callbacks, WSClientPool wsPool) {
        this.panel = new WSPanel();
        this.frame = new JFrame();
        frame.setContentPane(panel.getRootPanel());
        frame.pack();

        /**
         * Set event handlers for panel
         */
        this.handler = new WSPanelHandler(panel, callbacks, wsPool);
    }

    public WSPanel getPanel() {
        return panel;
    }

    public WSPanelHandler getHandler() {
        return handler;
    }

    @Override
    public String getTabCaption() {
        return "WebSockets";
    }

    @Override
    public Component getUiComponent() {
        return frame.getContentPane();
    }

    /**
     * Debug method, to check how gui creates
     * @param args
     */
    static public void main(String args[]) {
        JFrame frame = new JFrame("");
        frame.setContentPane(new WSPanel().getRootPanel());
        frame.pack();

        frame.setPreferredSize(new Dimension(1200, 1200));
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
