package com.pwn.sockets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import java.io.IOException;
import java.net.HttpCookie;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebSocket()
public class WSClient {

    static final Logger log = LogManager.getLogger(LocalServlet.class.getName());

    private URI uri;
    private Session session;
    private WebSocketClient client;
    private MessageHandler messageHandler;

    public WSClient(URI uri) {
        try {
            this.uri = uri;
            this.client = new WebSocketClient();
            client.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        log.info("Connection closed: " + statusCode + " - " + reason);
        this.session = null;
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        log.info("Got connect: " + session);
        this.session = session;
    }

    @OnWebSocketMessage
    public void onMessage(String message) {
        log.info("Got msg: " + message);
        if (this.messageHandler != null) {
            this.messageHandler.handleMessage(message);
            this.messageHandler.countDown();
        }
    }

    public void setMessageHandler(MessageHandler msgHandler) {
        this.messageHandler = msgHandler;
    }

    public MessageHandler getMessageHandler() {
        return messageHandler;
    }

    /**
     * Synding message with blocking operation
     */
    public void sendMessage(String message) {
        log.info("Try send message: " + message);
        try {
            session.getRemote().sendString(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(List<HttpCookie> cookies) {
        try {
            ClientUpgradeRequest request = new ClientUpgradeRequest();
            client.connect(this, uri, request);
            if (cookies != null) {
                request.setCookies(cookies);
            }
            log.info("Connecting to: " + uri);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Session getSession() {
        return session;
    }
}