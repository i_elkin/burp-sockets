package com.pwn.sockets;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static String extractHeaderValue(Pattern pattern, String request) {
        Matcher matcher = pattern.matcher(request);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }

    public static List<HttpCookie> extractCookiesList(String cookieString) {
        List<HttpCookie> cookies = new ArrayList<>();
        Arrays.stream(cookieString.split(";"))
                .forEach(s -> {
                    String[] c = s.split("=");
                    if (c.length > 1) {
                        cookies.add(new HttpCookie(c[0], c[1]));
                    }
                });

        return cookies;
    }
}
