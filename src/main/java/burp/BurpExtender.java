package burp;

import com.pwn.sockets.HttpListener;
import com.pwn.sockets.LocalServlet;
import com.pwn.sockets.WSClientPool;
import com.pwn.sockets.gui.Tab;
import com.pwn.sockets.gui.menu.MenuFactory;

public class BurpExtender implements IBurpExtender {

    @Override
    public void registerExtenderCallbacks(IBurpExtenderCallbacks callbacks) {

        callbacks.setExtensionName("Burp Sockets");

        /**
         * Sockets pool init
         */
        WSClientPool wsPool = new WSClientPool();


        Tab tab = new Tab(callbacks, wsPool);
        callbacks.addSuiteTab(tab);
        callbacks.registerHttpListener(new HttpListener(callbacks, tab, wsPool));

        callbacks.registerContextMenuFactory(new MenuFactory(callbacks, tab));

        /**
         * Register local servlet to make fake responses
         */
        new LocalServlet();
    }

}
